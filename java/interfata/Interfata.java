package interfata;

import operatii.Polinom;

import javax.swing.*;
import javax.xml.bind.annotation.XmlType;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

public class Interfata extends JFrame {
    public static void main(String[] args){
        JFrame frame=new JFrame("Polinom");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,300);
        JPanel panel1=new JPanel();
        JPanel panel2=new JPanel();
        JPanel panel3=new JPanel();
        JPanel panel4=new JPanel();
        JPanel panel5=new JPanel();
        JLabel label1=new JLabel("Polinom 1:");
        JLabel label2=new JLabel("Polinom 2:");
        Font font = new Font("Veranda",Font.ITALIC,12);
        Font anotherFont = new Font("Veranda",Font.PLAIN,12);
        JTextField p1=new JTextField("Introduceti primul polinom");
        p1.setFont(font);
        p1.setForeground(Color.GRAY);
        JTextField p2=new JTextField("Introduceti al doilea polinom");
        p2.setFont(font);
        p2.setForeground(Color.GRAY);
        p1.addMouseListener(new java.awt.event.MouseAdapter(){
           public void mouseClicked(java.awt.event.MouseEvent event){
               p1.setText("");
               p1.setFont(anotherFont);
               p1.setForeground(Color.BLACK);
           }
        });
        final boolean[] ok = {false,false};
        p1.addKeyListener(new java.awt.event.KeyAdapter(){
            public void keyPressed(java.awt.event.KeyEvent event){
                if(!ok[0]){
                    p1.setText("");
                    p1.setFont(anotherFont);
                    p1.setForeground(Color.BLACK);
                    ok[0] = true;
                }
            }
        });
        p2.addKeyListener(new java.awt.event.KeyAdapter(){
            public void keyPressed(java.awt.event.KeyEvent event){
                if(!ok[1]){
                    p2.setText("");
                    p2.setFont(anotherFont);
                    p2.setForeground(Color.BLACK);
                    ok[1] = true;
                }
            }
        });
        p2.addMouseListener(new java.awt.event.MouseAdapter(){
            public void mouseClicked(java.awt.event.MouseEvent event){
                p2.setText("");
                p2.setFont(anotherFont);
                p2.setForeground(Color.BLACK);
            }
        });
        JLabel rez=new JLabel("Rezultat:");
        JTextField rezt=new JTextField(20);
        panel1.add(label1);
        panel1.add(p1);
        panel2.add(label2);
        panel2.add(p2);
        panel3.add(panel1);
        panel3.add(panel2);
        panel4.add(rez);
        panel4.add(rezt);
        JButton btnAdd=new JButton("Suma");
        btnAdd.setBackground(Color.GRAY);
        panel5.add(btnAdd);
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom polinom=Polinom.buildPolinom(p1.getText());
                Polinom polinom1=Polinom.buildPolinom(p2.getText());
                rezt.setText(polinom.adunare(polinom1).toString());
            }
        });
        JButton btnScz=new JButton("Diferenta");
        btnScz.setBackground(Color.GRAY);
        panel5.add(btnScz);
        btnScz.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom polinom=Polinom.buildPolinom(p1.getText());
                Polinom polinom1=Polinom.buildPolinom(p2.getText());
                rezt.setText(polinom.scadere(polinom1).toString());
            }
        });
        JButton btnInm=new JButton("Inmultire");
        btnInm.setBackground(Color.GRAY);
        panel5.add(btnInm);
        btnInm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom polinom=Polinom.buildPolinom(p1.getText());
                Polinom polinom1=Polinom.buildPolinom(p2.getText());
                rezt.setText(polinom.inmultire(polinom1).toString());
            }
        });
        JButton btnDeriv=new JButton("Derivare");
        btnDeriv.setBackground(Color.GRAY);
        panel5.add(btnDeriv);
        btnDeriv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom polinom=Polinom.buildPolinom(p1.getText());
                rezt.setText(polinom.derivare().toString());
            }
        });
        JButton btnInt=new JButton("Integrare");
        btnInt.setBackground(Color.GRAY);
        panel5.add(btnInt);
        btnInt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom polinom=Polinom.buildPolinom(p1.getText());
                rezt.setText(polinom.integrare().toString());
            }
        });
        JPanel panel6=new JPanel();
        //panel6.add(panel4);
        //panel6.add(panel5);
        JPanel panel=new JPanel();
        panel.add(panel3);
        //panel.add(panel6);
        panel.add(panel4);
        panel.add(panel5);
        frame.setContentPane(panel);
        frame.setVisible(true);
    }

}