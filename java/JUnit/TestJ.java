package JUnit;

import operatii.Polinom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestJ{

    @Test
    public void testAdunare(){

        Polinom p1 = Polinom.buildPolinom("X^2 + 1");
        Polinom p2 = Polinom.buildPolinom("X + 1");

        Polinom rezultatCorect = Polinom.buildPolinom("X^2 + X + 2");
        Polinom rezultat = p1.adunare(p2);

        Assertions.assertEquals(rezultatCorect.toString(),rezultat.toString());

    }

    @Test
    public void testScadere(){
        Polinom p1 = Polinom.buildPolinom("X^2 + 1");
        Polinom p2 = Polinom.buildPolinom("X + 1");

        Polinom rezultatCorect = Polinom.buildPolinom("X^2 - X + 0");
        Polinom rezultat = p1.scadere(p2);

        Assertions.assertEquals(rezultatCorect.toString(),rezultat.toString());
    }

    @Test
    public void testInmultire(){
        Polinom p1 = Polinom.buildPolinom("X^2 + 1");
        Polinom p2 = Polinom.buildPolinom("X + 1");

        Polinom rezultatCorect = Polinom.buildPolinom("1X^3 + 1X^2 + 1X + 1");
        Polinom rezultat = p1.inmultire(p2);

        Assertions.assertEquals(rezultatCorect.toString(),rezultat.toString());
    }

    @Test
    public void testDerivare(){
        Polinom p1 = Polinom.buildPolinom("X^2 + 1");

        Polinom rezultatCorect = Polinom.buildPolinom("2X + 0");
        Polinom rezultat = p1.derivare();

        Assertions.assertEquals(rezultatCorect.toString(),rezultat.toString());
    }

    @Test
    public void testIntegrare(){
        Polinom p1 = Polinom.buildPolinom("X^2 + 1");

        Polinom rezultatCorect = Polinom.buildPolinom("0.3333333333333333X^3 + X");
        Polinom rezultat = p1.integrare();

        Assertions.assertEquals(rezultatCorect.toString(),rezultat.toString());
    }
}
