package operatii;

public class Monom {
    protected double coef;
    protected int exp;
    public Monom(double coef,int exp){
        this.exp=exp;
        this.coef=coef;
    }

    public void setExp(int exp){
        this.exp=exp;
    }

    public int getExp(){
        return this.exp;
    }

    public double getCoef(){
        return coef;
    }
    public void setCoef(double coef){
        this.coef=coef;
    }
    public String toString() {

        return this.show(false);
    }

    public String show(boolean abs) {
        String p = exp <= 0 ? "" : "X" + (exp == 1 ? "" : "^" + exp); // daca exp == 0 => nu pune X; daca exp != 0 => pune ( daca exp == 1 => nici putere nici X, daca exp != 1 => X^putere )
        String c = (abs ? Math.abs(coef) : coef) + ""; // il afisez ca nr pozitiv ca sa nu apara "2 +3" ci "2 + 3", sau nu "2 -7" ci "2 - 7", de semn ma ocup in clasa polinom
        if(c.endsWith(".0")) {
            c = c.replace(".0", "");
        }
        return c + p;
    }
}
