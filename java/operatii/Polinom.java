package operatii;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public class Polinom {
    public List<Monom> pol;
    public Polinom() {
        pol = new ArrayList<Monom>();
    }

    public List<Monom> getMonoame() {
        return pol;
    }

    public static Polinom buildPolinom(String data) {
        data = data.replaceAll(" ", "");
        Polinom out = new Polinom();
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            String found = matcher.group(0).toLowerCase();
            double coef;
            int put;
            if(found.contains("x^")) {
                if(found.charAt(0) == 'x' || found.substring(0,2).equalsIgnoreCase("-x")) {
                    coef = found.charAt(0) == 'x' ? 1 : -1;
                } else {
                    coef = Double.parseDouble(found.split("x\\^")[0]);
                }
                put = Integer.parseInt(found.split("x\\^")[1]);
            } else if(found.contains("x")) {
                if(found.charAt(0)=='x' || found.charAt(0) == '-' || found.charAt(0) == '+') {
                    coef = found.charAt(0) == 'x' ? 1 : found.charAt(0)=='+'? 1 : -1;
                } else {
                    coef = Double.parseDouble(found.split("x")[0]);
                }
                put = 1;
            } else {
                put = 0;
                coef = Double.parseDouble(found);
            }
            out.getMonoame().add( new Monom(coef, put) );
        }
        return out;
    }

    public Polinom adunare(Polinom p2) {
        Polinom rez=new Polinom();
        for (Monom m : this.pol) {
            rez.getMonoame().add(new Monom(m.getCoef(), m.getExp()));
        }
        for (Monom m : p2.getMonoame()) {
            boolean adunat = false;
            for (Monom m1 : rez.getMonoame()) {
                if (m.getExp() == m1.getExp()) {
                    m1.setCoef( m1.getCoef() + m.getCoef() );
                    adunat = true;
                }
            }
            if (!adunat) {
                rez.getMonoame().add(new Monom(m.getCoef(), m.getExp()));
            }
        }
        return rez;
    }

    public Polinom scadere(Polinom p2) {
        Polinom rez = new Polinom();
        for(Monom m : p2.getMonoame()) {
            rez.getMonoame().add(new Monom(m.getCoef() * -1, m.getExp()));
        }

        return adunare(rez);
    }

    public Polinom inmultire(Polinom p2){
        Polinom rez=new Polinom();
        Polinom rez1=new Polinom();
        for(Monom mrez1:this.pol){
            rez1.getMonoame().add(new Monom(mrez1.getCoef(),mrez1.getExp()));
        }
        for(Monom m : p2.getMonoame()) {
            for (Monom m1 : this.pol) {
                Monom aux = new Monom(0, 0);
                aux.setCoef(m1.getCoef() * m.getCoef());
                aux.setExp(m1.getExp() + m.getExp());
                rez.getMonoame().add(aux);
            }
        }
        return adunare(rez).scadere(rez1);
    }

    public Polinom derivare(){
        Polinom rez=new Polinom();
        for(Monom m : this.pol){
            rez.getMonoame().add(new Monom(m.getCoef(), m.getExp()));
        }
        for(Monom m1 : rez.getMonoame()){
            m1.setCoef(m1.getCoef()*m1.getExp());
            m1.setExp(m1.getExp()-1);
        }
        return rez;
    }

    public Polinom integrare(){
        Polinom rez=new Polinom();
        for(Monom m : this.pol){
            rez.getMonoame().add(new Monom(m.getCoef(), m.getExp()));
        }
        for(Monom m1 : rez.getMonoame()){
            m1.setCoef(m1.getCoef()*Math.pow(m1.getExp()+1,-1));
            m1.setExp(m1.getExp()+1);
        }
        return rez;
    }

    public String toString(){
        pol.sort((m1,m2)->Integer.compare(m2.getExp(),m1.getExp()));
        StringBuilder out=new StringBuilder("");
        boolean first=true;
        for(Monom monom:pol){
            if(first){
                out.append(monom);
                first=false;
            }
            else if(monom.getCoef()>=0){
                out.append("+");
                out.append(monom);
            }
            else{
                out.append(monom);
            }
        }
        return out.toString();
    }
}
